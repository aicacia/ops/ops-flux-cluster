ops-flux-cluster
=====

## Getting Started

1. replace email `example@mail.com`
2. replace domains example `registry.example.com` and tls secret names example `registry-example-com-tls`
3. generate htpasswd for docker `htpasswd -b -c .htpasswd user password` replace the default value in the docker-registry yaml